import datetime

import requests
import json
import numpy as np
import pandas as pd

from bokeh.plotting import figure, output_file, save, show
from bokeh.models import ColumnDataSource, HoverTool, GeoJSONDataSource
from bokeh.tile_providers import CARTODBPOSITRON_RETINA, get_provider

url ='https://api.aprs.fi/api/get?name=234637000,234623000,234605000&what=loc&apikey=141859.L2LYSa9Z7oGeD&format=json'

tools       = 'pan, wheel_zoom, reset'

def getData():
    json_data = requests.get(url).json()
    return json_data

# Function to convert Lat, Long to Web Mercator
def geographic_to_web_mercator(lat, long):
    num = long * 0.017453292519943295
    x = 6378137.0 * num
    a = lat * 0.017453292519943295
    x_mercator = x
    y_mercator = 3189068.5 * np.log((1.0 + np.sin(a)) / (1.0 - np.sin(a)))
    return x_mercator, y_mercator

def plotPosition(json_data):
    # Get the tile provider
    tile_provider = get_provider(CARTODBPOSITRON_RETINA)

    lats = []
    lngs = []
    names = []
    times = []

    for entry in json_data['entries']:

        lats.append(float(entry['lat']))
        lngs.append(float(entry['lng']))
        names.append(entry['name'])
        times.append(datetime.datetime.fromtimestamp(int(entry['time'])))

    df = pd.DataFrame()

    df['lats'] = lats
    df['lngs'] = lngs
    df['names'] = names
    df['times'] = times

    df['WM_x'], df['WM_y'] = geographic_to_web_mercator(df['lats'], df['lngs'])

    print(df)

    x_min = df['WM_x'].min() - 100000
    x_max = df['WM_x'].max() + 100000
    y_min = df['WM_y'].min() - 100000
    y_max = df['WM_y'].max() + 100000

    # Create the plot
    plot = figure(x_range=(x_min, x_max), 
                  y_range=(y_min, y_max),
                  x_axis_type="mercator", 
                  y_axis_type="mercator",
                  title='Where is HMS Mersey', 
                  tools = tools,
                  active_drag   = 'pan',
                  active_scroll = 'wheel_zoom')

    # Add the tile streamer to the plot
    plot.add_tile(tile_provider)

    # Set the sizing mode
    plot.sizing_mode = 'stretch_both'

    # Setup the tooltips
    tooltips = [('Name', '@names'),
                ('Latitude', '@lats'),
                ('Longitude', '@lngs'),
                ('Last Timestamp', '@times{%c}')]

    formatter = { '@times' : 'datetime' }

    # Create a hover tool
    hover_tool = HoverTool()

    # Set the tooltips
    hover_tool.tooltips = tooltips
    hover_tool.formatters = formatter
    
    # Add the tooltip
    plot.add_tools(hover_tool)

    cds = ColumnDataSource.from_df(df)

    # Add the data to the circle plot
    plot.circle(x='WM_x', 
                 y='WM_y',
                source=cds,
                 size=20)

    return plot

def main():
    output_file('index.html', 'Where is HMS Mersey?')

    json_data = getData()

    plot = plotPosition(json_data)

    show(plot)

main()